/*
 * transsip - the telephony toolkit
 * By Daniel Borkmann <daniel@transsip.org>
 * Copyright 2012 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL, version 2.
 * Based on Bhaskar Biswas and Nicolas Sendrier McEliece
 * implementation, LGPL 2.1.
 */

#include <string.h>

#include "scramble.h"
#include "sizes.h"

/*
 * TODO:
 *  This function is aimed at randomizing the cleartext to provide
 *  semantic security. Here, it is a fake and we just make a copy.
 *
 *  In a real conversion the message is first padded with random bytes
 *  and zeroes then shuffled, for instance with a 3 round
 *  "Fiestel-like" scheme. The message of size MESSAGE_BYTES bytes is
 *  transformed into a randomized block of size CLEARTEXT_LENGTH
 *  bits.
 */

void scramble(uint8_t *cleartext, uint8_t *message)
{
	memcpy(cleartext, message, MESSAGE_BYTES);
}

int unscramble(uint8_t *message, uint8_t *cleartext)
{
	memcpy(message, cleartext, MESSAGE_BYTES);
	return 1;
}
