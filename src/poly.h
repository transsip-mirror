/*
 * transsip - the telephony toolkit
 * By Daniel Borkmann <daniel@transsip.org>
 * Copyright 2012 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL, version 2.
 * Based on Bhaskar Biswas and Nicolas Sendrier McEliece
 * implementation, LGPL 2.1.
 */

#ifndef POLY_H
#define POLY_H

#include "gf.h"

struct polynomial {
	int deg, size;
	gf16_t *coeff; /* polynomial has coefficients in the finite field */
};

#ifndef TRUE
# define TRUE	1
# define FALSE	0
#endif

#define poly_deg(p)			((p)->deg)
#define poly_size(p)			((p)->size)
#define poly_set_deg(p, d)		((p)->deg = (d))
#define poly_coeff(p, i)		((p)->coeff[i])
#define poly_set_coeff(p, i, a)		((p)->coeff[i] = (a))
#define poly_addto_coeff(p, i, a) 	((p)->coeff[i] = gf_add((p)->coeff[i], (a)))
#define poly_multo_coeff(p, i, a) 	((p)->coeff[i] = gf_mul((p)->coeff[i], (a)))
#define poly_tete(p)			((p)->coeff[(p)->deg])

extern int poly_calcule_deg(struct polynomial *p);
extern struct polynomial *poly_alloc(int d);
extern struct polynomial *
poly_alloc_from_string(int d, const unsigned char *str, size_t len);
extern struct polynomial *poly_copy(struct polynomial *p);
extern void poly_free(struct polynomial *p);
extern void poly_set_to_zero(struct polynomial *p);
extern void poly_set(struct polynomial *p, struct polynomial *q);
extern struct polynomial *poly_mul(struct polynomial *p, struct polynomial *q);
extern void poly_rem(struct polynomial *p, struct polynomial *g);
extern void poly_sqmod_init(struct polynomial *g, struct polynomial **sq);
extern void poly_sqmod(struct polynomial *res, struct polynomial *p,
		       struct polynomial **sq, int d);
extern struct polynomial *poly_gcd(struct polynomial *p1,
				   struct polynomial *p2);
extern struct polynomial *poly_quo(struct polynomial *p,
				   struct polynomial *d);
extern gf16_t poly_eval(struct polynomial *p, gf16_t a);
extern int poly_degppf(struct polynomial *g);
extern void poly_eeaux(struct polynomial **u, struct polynomial **v,
		       struct polynomial *p, struct polynomial *g, int t);
extern struct polynomial **poly_syndrome_init(struct polynomial *generator,
					      gf16_t *support, int n);
extern struct polynomial **poly_sqrtmod_init(struct polynomial *g);
extern struct polynomial *poly_randgen_irred(int t, int (*u8rnd)(void));

#endif /* POLY_H */
