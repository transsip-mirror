/*
 * transsip - the telephony toolkit
 * By Daniel Borkmann <daniel@transsip.org>
 * Copyright 2012 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL, version 2.
 * Based on Bhaskar Biswas and Nicolas Sendrier McEliece
 * implementation, LGPL 2.1.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gf.h"
#include "poly.h"
#include "xmalloc.h"

struct polynomial *poly_alloc(int d)
{
	struct polynomial *p = NULL;

	p = xmalloc(sizeof(*p));
	p->deg = -1;
	p->size = d + 1;
	p->coeff = xzmalloc(p->size * sizeof(gf16_t));

	return p;
}

struct polynomial *poly_alloc_from_string(int d, const unsigned char *str,
					  size_t len)
{
	struct polynomial *p = NULL;

	p = xmalloc(sizeof(*p));
	p->deg = -1;
	p->size = d + 1;
	p->coeff = xmemdup(str, len);

	bug_on(p->size * sizeof(gf16_t) != len);

	return p;
}

struct polynomial *poly_copy(struct polynomial *p)
{
	struct polynomial *q = NULL;

	q = xmalloc(sizeof(*q));
	q->deg = p->deg;
	q->size = p->size;
	q->coeff = xzmalloc(q->size * sizeof(gf16_t));

	memcpy(q->coeff, p->coeff, p->size * sizeof(gf16_t));

	return q;
}

void poly_free(struct polynomial *p)
{
	xfree(p->coeff);
	xfree(p);
}

void poly_set_to_zero(struct polynomial *p)
{
	memset(p->coeff, 0, p->size * sizeof(gf16_t));
	p->deg = -1;
}

int poly_calcule_deg(struct polynomial *p)
{
	int d = p->size - 1;

	while ((d >= 0) && (p->coeff[d] == gf_zero()))
		--d;
	p->deg = d;

	return d;
}

/* copy q in p */
void poly_set(struct polynomial *p, struct polynomial *q)
{
	int d = p->size - q->size;

	if (d < 0) {
		memcpy(p->coeff, q->coeff, p->size * sizeof(gf16_t));
		poly_calcule_deg(p);
	} else {
		memcpy(p->coeff, q->coeff, q->size * sizeof(gf16_t));
		memset(p->coeff + q->size, 0, d * sizeof(gf16_t));
		p->deg = q->deg;
	}
}

static gf16_t poly_eval_aux(gf16_t *coeff, gf16_t a, int d)
{
	gf16_t b;

	b = coeff[d--];
	for (; d >= 0; --d) {
		if (b != gf_zero())
			b = gf_add(gf_mul(b, a), coeff[d]);
		else
			b = coeff[d];
	}

	return b;
}

struct polynomial *poly_mul(struct polynomial *p, struct polynomial *q)
{
	int i, j, dp, dq;
	struct polynomial *r;

	poly_calcule_deg(p);
	poly_calcule_deg(q);

	dp = poly_deg(p);
	dq = poly_deg(q);

	r = poly_alloc(dp + dq);

	for (i = 0; i <= dp; ++i)
		for (j = 0; j <= dq; ++j)
			poly_addto_coeff(r, i + j, gf_mul(poly_coeff(p, i),
					 poly_coeff(q, j)));
	poly_calcule_deg(r);

	return r;
}

gf16_t poly_eval(struct polynomial *p, gf16_t a)
{
	return poly_eval_aux(p->coeff, a, poly_deg(p));
}

/* p contain it's remainder modulo g */
void poly_rem(struct polynomial *p, struct polynomial *g)
{
	int i, j, d;
	gf16_t a, b;

	d = poly_deg(p) - poly_deg(g);
	if (d >= 0) {
		a = gf_inv(poly_tete(g));

		for (i = poly_deg(p); d >= 0; --i, --d) {
			if (poly_coeff(p, i) != gf_zero()) {
				b = gf_mul_fast(a, poly_coeff(p, i));

				for (j = 0; j < poly_deg(g); ++j) {
					poly_addto_coeff(p, j + d,
						gf_mul_fast(b, poly_coeff(g, j)));
				}

				poly_set_coeff(p, i, gf_zero());
			}
		}

		poly_set_deg(p, poly_deg(g) - 1);

		while ((poly_deg(p) >= 0) &&
		       (poly_coeff(p, poly_deg(p)) == gf_zero()))
			poly_set_deg(p, poly_deg(p) - 1);
	}
}

void poly_sqmod_init(struct polynomial *g, struct polynomial **sq)
{
	int i, d;

	d = poly_deg(g);

	for (i = 0; i < d / 2; ++i) {
		/* sq[i] = x^(2i) mod g = x^(2i) */
		poly_set_to_zero(sq[i]);
		poly_set_deg(sq[i], 2 * i);
		poly_set_coeff(sq[i], 2 * i, gf_unit());
	}

	for (; i < d; ++i) {
		/* sq[i] = x^(2i) mod g = (x^2 * sq[i-1]) mod g */
		memset(sq[i]->coeff, 0, 2 * sizeof(gf16_t));
		memcpy(sq[i]->coeff + 2, sq[i - 1]->coeff, d * sizeof(gf16_t));
		poly_set_deg(sq[i], poly_deg(sq[i - 1]) + 2);
		poly_rem(sq[i], g);
	}
}

/*
 * Modulo p square of a certain polynomial g, sq[] contains the square
 * Modulo g of the base canonical polynomials of degree < d, where d is
 * the degree of G. The table sq[] will be calculated by poly_sqmod_init
 */
void poly_sqmod(struct polynomial *res, struct polynomial *p,
		struct polynomial **sq, int d)
{
	int i, j;
	gf16_t a;

	poly_set_to_zero(res);

	/* terms of low degree */
	for (i = 0; i < d / 2; ++i)
		poly_set_coeff(res, i * 2, gf_square(poly_coeff(p, i)));

	/* terms of high degree */
	for (; i < d; ++i) {
		if (poly_coeff(p, i) != gf_zero()) {
			a = gf_square(poly_coeff(p, i));

			for (j = 0; j < d; ++j)
				poly_addto_coeff(res, j,
					gf_mul_fast(a, poly_coeff(sq[i], j)));
		}
	}

	/* update degre */
	poly_set_deg(res, d - 1);

	while ((poly_deg(res) >= 0) &&
	       (poly_coeff(res, poly_deg(res)) == gf_zero()))
		poly_set_deg(res, poly_deg(res) - 1);
}

/* destructive */
static struct polynomial *poly_gcd_aux(struct polynomial *p1,
				       struct polynomial *p2)
{
	if (poly_deg(p2) == -1)
		return p1;
	else {
		poly_rem(p1, p2);
		return poly_gcd_aux(p2, p1);
	}
}

struct polynomial *poly_gcd(struct polynomial *p1, struct polynomial *p2)
{
	struct polynomial *a, *b, *c;

	a = poly_copy(p1);
	b = poly_copy(p2);

	if (poly_deg(a) < poly_deg(b))
		c = poly_copy(poly_gcd_aux(b, a));
	else
		c = poly_copy(poly_gcd_aux(a, b));

	poly_free(a);
	poly_free(b);

	return c;
}

struct polynomial *poly_quo(struct polynomial *p, struct polynomial *d)
{
	int i, j, dd, dp;
	gf16_t a, b;
	struct polynomial *quo, *rem;

	dd = poly_calcule_deg(d);
	dp = poly_calcule_deg(p);

	rem = poly_copy(p);
	quo = poly_alloc(dp - dd);

	poly_set_deg(quo, dp - dd);

	a = gf_inv(poly_coeff(d, dd));

	for (i = dp; i >= dd; --i) {
		b = gf_mul_fast(a, poly_coeff(rem, i));

		poly_set_coeff(quo, i - dd, b);
		if (b != gf_zero()) {
			poly_set_coeff(rem, i, gf_zero());

			for (j = i - 1; j >= i - dd; --j)
				poly_addto_coeff(rem, j,
					gf_mul_fast(b, poly_coeff(d, dd - i + j)));
		}
	}

	poly_free(rem);

	return quo;
}

/* returns the degree of the smallest factor */
int poly_degppf(struct polynomial *g)
{
	int i, d, res;
	struct polynomial **u, *p, *r, *s;

	d = poly_deg(g);

	u = malloc(d * sizeof(*u));
	for (i = 0; i < d; ++i)
		u[i] = poly_alloc(d + 1);

	poly_sqmod_init(g, u);

	p = poly_alloc(d - 1);
	poly_set_deg(p, 1);
	poly_set_coeff(p, 1, gf_unit());

	r = poly_alloc(d - 1);
	res = d;

	for (i = 1; i <= (d / 2) * gf_extd(); ++i) {
		poly_sqmod(r, p, u, d);

		/* r = x^(2^i) mod g */
		if ((i % gf_extd()) == 0) {
			/* so 2^i = (2^m)^j (m ext. degree) */
			poly_addto_coeff(r, 1, gf_unit());
			poly_calcule_deg(r); /* the degree may change */

			s = poly_gcd(g, r);
			if (poly_deg(s) > 0) {
				poly_free(s);
				res = i / gf_extd();
				break;
			}

			poly_free(s);
			poly_addto_coeff(r, 1, gf_unit());
			poly_calcule_deg(r); /* the degree may change */
		}

		/* no need for the exchange s */
		s = p;
		p = r;
		r = s;
	}

	poly_free(p);
	poly_free(r);

	for (i = 0; i < d; ++i)
		poly_free(u[i]);
	xfree(u);

	return res;
}

/* we suppose deg(g) >= deg(p) */
void poly_eeaux(struct polynomial **u, struct polynomial **v,
		struct polynomial *p, struct polynomial *g, int t)
{
	int i, j, dr, du, delta;
	gf16_t a;
	struct polynomial *aux, *r0, *r1, *u0, *u1;

	/* initialisation of the local variables */
	/* r0 <- g, r1 <- p, u0 <- 0, u1 <- 1 */
	dr = poly_deg(g);

	r0 = poly_alloc(dr);
	r1 = poly_alloc(dr - 1);
	u0 = poly_alloc(dr - 1);
	u1 = poly_alloc(dr - 1);

	poly_set(r0, g);
	poly_set(r1, p);
	poly_set_to_zero(u0);
	poly_set_to_zero(u1);
	poly_set_coeff(u1, 0, gf_unit());
	poly_set_deg(u1, 0);

	/* invariants:
	 * r1 = u1 * p + v1 * g
	 * r0 = u0 * p + v0 * g
	 * and deg(u1) = deg(g) - deg(r0)
	 * it stops when deg (r1) <t (deg (r0)> = t)
	 * and therefore deg (u1) = deg (g) - deg (r0) <deg (g) - t
	 */

	du = 0;
	dr = poly_deg(r1);
	delta = poly_deg(r0) - dr;

	while (dr >= t) {
		for (j = delta; j >= 0; --j) {
			a = gf_div(poly_coeff(r0, dr + j),
				   poly_coeff(r1, dr));
			if (a != gf_zero()) {
				/* u0(z) <- u0(z) + a * u1(z) * z^j */
				for (i = 0; i <= du; ++i)
					poly_addto_coeff(u0, i + j,
						gf_mul_fast(a, poly_coeff(u1, i)));

				/* r0(z) <- r0(z) + a * r1(z) * z^j */
				for (i = 0; i <= dr; ++i)
					poly_addto_coeff(r0, i + j,
						gf_mul_fast(a, poly_coeff(r1, i)));
			}
		}

		/* exchange */
		aux = r0;
		r0 = r1;
		r1 = aux;

		aux = u0;
		u0 = u1;
		u1 = aux;

		du = du + delta;
		delta = 1;
		while (poly_coeff(r1, dr - delta) == gf_zero())
			delta++;
		dr -= delta;
	}

	poly_set_deg(u1, du);
	poly_set_deg(r1, dr);

	/* return u1 and r1 */
	*u = u1;
	*v = r1;

	poly_free(r0);
	poly_free(u0);
}

/* the field is already defined
 * return a polynomial of degree t irreducible in the field */
struct polynomial *poly_randgen_irred(int t, int (*u8rnd)(void))
{
	int i;
	struct polynomial *g;

	g = poly_alloc(t);
	poly_set_deg(g, t);
	poly_set_coeff(g, t, gf_unit());

	i = 0;
	do {
		for (i = 0; i < t; ++i)
			poly_set_coeff(g, i, gf_rand(u8rnd));
	} while (poly_degppf(g) < t);

	return g;
}

/* p = p * x mod g
 * p of degree <= deg(g)-1 */
static void poly_shiftmod(struct polynomial *p, struct polynomial *g)
{
	int i, t;
	gf16_t a;

	t = poly_deg(g);
	a = gf_div(p->coeff[t - 1], g->coeff[t]);

	for (i = t - 1; i > 0; --i)
		p->coeff[i] = gf_add(p->coeff[i - 1],
				     gf_mul(a, g->coeff[i]));
	p->coeff[0] = gf_mul(a, g->coeff[0]);
}

struct polynomial **poly_sqrtmod_init(struct polynomial *g)
{
	int i, t;
	struct polynomial **sqrt, *aux, *p, *q, **sq_aux;

	t = poly_deg(g);

	sq_aux = xmalloc(t * sizeof(*sq_aux));
	for (i = 0; i < t; ++i)
		sq_aux[i] = poly_alloc(t + 1);
	poly_sqmod_init(g, sq_aux);

	q = poly_alloc(t - 1);
	p = poly_alloc(t - 1);
	poly_set_deg(p, 1);

	poly_set_coeff(p, 1, gf_unit());
	/* q(z) = 0, p(z) = z */
	for (i = 0; i < t * gf_extd() - 1; ++i) {
		/* q(z) <- p(z)^2 mod g(z) */
		poly_sqmod(q, p, sq_aux, t);
		/* q(z) <-> p(z) */
		aux = q;
		q = p;
		p = aux;
	}
	/* p(z) = z^(2^(tm-1)) mod g(z) = sqrt(z) mod g(z) */

	sqrt = xmalloc(t * sizeof(*sqrt));
	for (i = 0; i < t; ++i)
		sqrt[i] = poly_alloc(t - 1);

	poly_set(sqrt[1], p);
	poly_calcule_deg(sqrt[1]);

	for(i = 3; i < t; i += 2) {
		poly_set(sqrt[i], sqrt[i - 2]);
		poly_shiftmod(sqrt[i], g);
		poly_calcule_deg(sqrt[i]);
	}

	for (i = 0; i < t; i += 2) {
		poly_set_to_zero(sqrt[i]);
		sqrt[i]->coeff[i / 2] = gf_unit();
		sqrt[i]->deg = i / 2;
	}

	for (i = 0; i < t; ++i)
		poly_free(sq_aux[i]);
	xfree(sq_aux);

	poly_free(p);
	poly_free(q);

	return sqrt;
}

struct polynomial **poly_syndrome_init(struct polynomial *generator,
				       gf16_t *support, int n)
{
	int i, j, t;
	gf16_t a;
	struct polynomial **F;

	F = xmalloc(n * sizeof(*F));
	t = poly_deg(generator);

	/* g(z)=g_t+g_(t-1).z^(t-1)+......+g_1.z+g_0 */
	/* f(z)=f_(t-1).z^(t-1)+......+f_1.z+f_0 */

	for (j = 0; j < n; ++j) {
		F[j] = poly_alloc(t - 1);
		poly_set_coeff(F[j], t - 1, gf_unit());

		for (i = t - 2; i >= 0; --i) {
			poly_set_coeff(F[j], i,
				       gf_add(poly_coeff(generator, i + 1),
					      gf_mul(support[j],
						     poly_coeff(F[j], i + 1))));
		}

		a = gf_add(poly_coeff(generator, 0),
			   gf_mul(support[j], poly_coeff(F[j], 0)));

		for (i = 0; i < t; ++i) {
			poly_set_coeff(F[j], i, gf_div(poly_coeff(F[j], i), a));
		}
	}

	return F;
}
