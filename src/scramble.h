/*
 * transsip - the telephony toolkit
 * By Daniel Borkmann <daniel@transsip.org>
 * Copyright 2012 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL, version 2.
 * Based on Bhaskar Biswas and Nicolas Sendrier McEliece
 * implementation, LGPL 2.1.
 */

#ifndef SCRAMBLE_H
#define SCRAMBLE_H

#include <stdint.h>

extern void scramble(uint8_t *cleartext, uint8_t *message);
extern int unscramble(uint8_t *message, uint8_t *cleartext);

#endif /* SCRAMBLE_H */
