/*
 * transsip - the telephony toolkit
 * By Daniel Borkmann <daniel@transsip.org>
 * Copyright 2012 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL, version 2.
 * Based on Bhaskar Biswas and Nicolas Sendrier McEliece
 * implementation, LGPL 2.1.
 */

#ifndef MAT_H
#define MAT_H

#include <stdint.h>

#define BITS_PER_LONG	__WORDSIZE

struct matrix {
	size_t rown;		/* Number of rows */
	size_t coln;		/* Number of columns */
	size_t rwdcnt;		/* Number of words in a row */
	size_t alloc_size;	/* Number of allocated bytes */
	uint32_t *elem;		/* Row index */
};

#define matrix_coeff(A, i, j)					\
	(((A)->elem[(i) * A->rwdcnt +				\
	  (j) / BITS_PER_LONG] >> ((j) % BITS_PER_LONG)) & 1)

#define matrix_set_coeff_to_one(A, i, j)			\
	((A)->elem[(i) * A->rwdcnt +				\
	 (j) / BITS_PER_LONG] |= (1UL << ((j) % BITS_PER_LONG)))

#define matrix_change_coeff(A, i, j)				\
	((A)->elem[(i) * A->rwdcnt +				\
	 (j) / BITS_PER_LONG] ^= (1UL << ((j) % BITS_PER_LONG)))

#define matrix_set_to_zero(R)	memset((R)->elem, 0, (R)->alloc_size);

extern struct matrix *matrix_init(size_t rown, size_t coln);
extern struct matrix *matrix_init_from_string(size_t rown, size_t coln,
					      const unsigned char *str,
					       size_t len);
extern void matrix_free(struct matrix *A);
extern struct matrix *matrix_copy(struct matrix *A);
extern struct matrix *matrix_rowxor(struct matrix *A, int a, int b);
extern uint32_t *matrix_rref(struct matrix *A);
extern void matrix_vec_mul(uint32_t *cR, uint8_t *x, struct matrix *A);
extern struct matrix *matrix_mul(struct matrix *A, struct matrix *B);

#endif /* MAT_H */
