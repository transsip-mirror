/*
 * transsip - the telephony toolkit
 * By Daniel Borkmann <daniel@transsip.org>
 * Copyright 2012 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL, version 2.
 * Based on Bhaskar Biswas and Nicolas Sendrier McEliece
 * implementation, LGPL 2.1.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mat.h"
#include "xmalloc.h"

struct matrix *matrix_init(size_t rown, size_t coln)
{
	struct matrix *A = NULL;

	A = xzmalloc(sizeof(*A));
	A->coln = coln;
	A->rown = rown;  
	A->rwdcnt = (1 + (coln - 1) / BITS_PER_LONG);
	A->alloc_size = rown * A->rwdcnt * sizeof(uint32_t);
	A->elem = xzmalloc(A->alloc_size);

	return A;
}

struct matrix *matrix_init_from_string(size_t rown, size_t coln,
				       const unsigned char *str, size_t len)
{
	struct matrix *A = NULL;

	A = xzmalloc(sizeof(*A));
	A->coln = coln;
	A->rown = rown;  
	A->rwdcnt = (1 + (coln - 1) / BITS_PER_LONG);
	A->alloc_size = rown * A->rwdcnt * sizeof(uint32_t);
	A->elem = xmemdup(str, len);

	bug_on(A->alloc_size != len);

	return A;
}

void matrix_free(struct matrix *A)
{
	xfree(A->elem);
	xfree(A);
}

struct matrix *matrix_copy(struct matrix *A)
{
	int i;
	struct matrix *X;
  
	X = matrix_init(A->rown, A->coln);

	for (i = 0; i < A->rwdcnt * A->rown; ++i)
		X->elem[i] = A->elem[i];

	return X;
}

struct matrix *matrix_rowxor(struct matrix *A, int a, int b)
{
	int i;

	for (i = 0; i < A->rwdcnt; ++i)
		A->elem[a * A->rwdcnt + i] ^= A->elem[b * A->rwdcnt + i];

	return A;
}

void matrix_vec_mul(uint32_t *cR, uint8_t *x, struct matrix *A)
{
	int i,j;
	uint32_t *pt;

	memset(cR, 0, A->rwdcnt * sizeof(uint32_t));
	pt = A->elem;

	/* extract the first column in the form of char array */
	for (i = 0; i < A->rown; ++i) {
		if ((x[i / 8] >> (i % 8)) & 1) {
			for (j = 0; j < A->rwdcnt; ++j)
				cR[j] ^= *pt++;
		} else {
			pt += A->rwdcnt;
		}
	}
}

struct matrix *matrix_mul(struct matrix *A, struct matrix *B)
{
	int i, j, k;
	struct matrix *C;

	bug_on(A->coln != B->rown);

	C = matrix_init(A->rown, B->coln);
	memset(C->elem, 0, C->alloc_size);

	for (i = 0; i <A->rown; ++i)
		for (j = 0; j < B->coln; ++j)
			for (k = 0; k < A->coln; ++k)
				if (matrix_coeff(A, i, k) &&
				    matrix_coeff(B, k, j))
					matrix_change_coeff(C, i, j);
	return C;
}

uint32_t *matrix_rref(struct matrix *A)
{
	/* the matrix is reduced from LSB...(from right) */
	uint32_t *perm;
	int i, j, failcnt, findrow, max;

	max = A->coln - 1;
	perm = xmalloc(A->coln * sizeof(uint32_t));

	for (i = 0; i < A->coln; ++i)
		perm[i] = i; /* initialize permutation */
	failcnt = 0;

	for (i = 0; i < A->rown; ++i,--max) {
		findrow = 0;

		for (j = i; j < A->rown; ++j) {
			if(matrix_coeff(A, j, max)) {
				/* not needed as ith row is 0 and jth row is 1 */
	      			if (i != j) /* xor to the row.(swap)? */
					A = matrix_rowxor(A, i, j);
				findrow = 1;
				break;
			}
		}

		/* if no row with a 1 found then swap last column and
		 * the column with no 1 down */
		if (!findrow) {
			perm[A->coln - A->rown - 1 - failcnt] = max;
			failcnt++;

			if (!max)
				return NULL;
			i--;
		} else {
			perm[i + A->coln - A->rown] = max;
			/* fill the column downwards with 0's */
			for (j = i + 1; j < A->rown; ++j) {
				if (matrix_coeff(A, j, max))
					/* check the arg. order */
					A = matrix_rowxor(A, j, i);
			}

			/*  fill the column with 0's upwards too */
			for (j = i - 1; j >= 0; --j) {
				if (matrix_coeff(A, j, max))
					A = matrix_rowxor(A, j, i);
			}
		}
	}

	return perm;
}
