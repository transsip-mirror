#
# transsip - the telephony toolkit
# By Emmanuel Roullit <emmanuel@netsniff-ng.org>
# Copyright 2012 Emmanuel Roullit.
# Subject to the GPL, version 2.
#/

# - Try to find CELT
# Once done this will define
#
#  CELT_FOUND - system has CELT
#  CELT_INCLUDE_DIRS - the CELT include directory
#  CELT_LIBRARY - Link these to use CELT
#  CELT_DEFINITIONS - Compiler switches required for using CELT
#

find_package(PkgConfig)
pkg_check_modules(PC_CELT QUIET celt)

find_path(CELT_INCLUDE_DIR celt/celt.h
          HINTS ${PC_CELT_INCLUDEDIR} ${PC_CELT_INCLUDE_DIRS})
find_library(CELT_LIBRARY celt0
             HINTS ${PC_CELT_LIBDIR} ${PC_CELT_LIBRARY_DIRS})

set(CELT_LIBRARIES ${CELT_LIBRARY})
set(CELT_INCLUDE_DIRS ${CELT_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CELT DEFAULT_MSG CELT_LIBRARY CELT_INCLUDE_DIR)
mark_as_advanced(CELT_INCLUDE_DIR CELT_LIBRARY)
